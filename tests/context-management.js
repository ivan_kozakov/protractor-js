var LoginPage = require('../pages/login-page.js');
var WorkspaceSelectionPage = require('../pages/workspace-selection-page.js');
var HomePage = require('../pages/home-page.js');
var ItemPopup = require('../pages/blocks/item-pop-up.js');
var ItemBlock = require('../pages/blocks/item-block.js');

describe('Context Management', function () {

    it('should create a context', function () {
        var loginPage = new LoginPage();
        loginPage.login();

        var workspaceSelectionPage = new WorkspaceSelectionPage();
        workspaceSelectionPage.selectWorkspace('AutoTest');

        var homePage = new HomePage();
        homePage.initiateItemAdding();

        var itemPopup = new ItemPopup();
        itemPopup.switchToContextTab();
        itemPopup.fillTitle('Context Title');
        itemPopup.fillDescription('Description');
        itemPopup.saveItem();
        expect(element(by.linkText('Context Title')).isDisplayed()).toBe(true);
    });

    it('should delete a context', function () {
        var homePage = new HomePage();
        homePage.enableEditMode();

        var itemBlock = new ItemBlock();
        itemBlock.deleteItem('Context Title');
        expect(element(by.linkText('Context Title')).isPresent()).toBe(false);
    });
});