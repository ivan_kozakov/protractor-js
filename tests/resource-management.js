var LoginPage = require('../pages/login-page.js');
var WorkspaceSelectionPage = require('../pages/workspace-selection-page.js');
var HomePage = require('../pages/home-page.js');
var ItemPopup = require('../pages/blocks/item-pop-up.js');
var ItemBlock = require('../pages/blocks/item-block.js');

describe('Resource Management', function () {

    it('should create a resource', function () {
        var loginPage = new LoginPage();
        loginPage.login();

        var workspaceSelectionPage = new WorkspaceSelectionPage();
        workspaceSelectionPage.selectWorkspace('AutoTest');

        var homePage = new HomePage();
        homePage.initiateItemAdding();

        var itemPopup = new ItemPopup();
        itemPopup.fillUrl('https://url.example.com');
        itemPopup.fillTitle('Resource Title');
        itemPopup.fillDescription('Description');
        itemPopup.saveItem();
        expect(element(by.linkText('Resource Title')).isDisplayed()).toBe(true);
    });

    it('should delete a resource', function () {
        var homePage = new HomePage();
        homePage.enableEditMode();

        var itemBlock = new ItemBlock();
        itemBlock.deleteItem('Resource Title');
        expect(element(by.linkText('Resource Title')).isPresent()).toBe(false);
    });
});