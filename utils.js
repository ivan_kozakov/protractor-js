var Utils = function () {

    this.clickByElementWhenReady = function (elm) {
        browser.wait(function () {
            return elm.isPresent();
        },10000);
        browser.wait(function () {
            return elm.isDisplayed();
        },10000);
        elm.click();
    };

    this.clickByLocatorWhenReady = function (locator) {
        browser.wait(function () {
            return browser.isElementPresent(locator);
        },10000);
        element(locator).click();
    };
};

module.exports = Utils;