var HtmlReporter = require('protractor-html-screenshot-reporter');

var reporter = new HtmlReporter({
    baseDirectory: './tmp/screenshots', // a location to store screen shots.
    docTitle: 'Protractor Demo Reporter',
    docName: 'protractor-demo-tests-report.html'
});

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['tests/resource-management.js', 'tests/context-management.js'],

    capabilities: {
        browserName: 'chrome',
        shardTestFiles: true,
        maxInstances: 2
    },

    onPrepare: function () {
        jasmine.getEnv().addReporter(reporter);
    }
};