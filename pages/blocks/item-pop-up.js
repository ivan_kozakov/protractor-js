var Utils = require('../../utils.js');

var ItemPopup = function() {
    this.url = element(by.id('ed-resource-modal-url-field'));
    this.title = element(by.id('ed-resource-modal-title-field'));
    this.description = element(by.id('ed-resource-modal-description-field'));
    this.contextTab = $("li[active='ed.is_group_tab']");
    this.saveButton = element(by.id('ed-resource-modal-save-button'));

    var utils = new Utils();

    this.fillUrl = function(url) {
        this.url.sendKeys(url);
    };

    this.fillTitle = function(title) {
        this.title.sendKeys(title);
    };

    this.fillDescription = function(description) {
        this.description.sendKeys(description);
    };

    this.switchToContextTab = function() {
        utils.clickByElementWhenReady(this.contextTab);
    };

    this.saveItem = function() {
        utils.clickByElementWhenReady(this.saveButton);
    };
};

module.exports = ItemPopup;