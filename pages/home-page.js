var Utils = require('../utils.js');

var HomePage = function () {
    this.addItemIcon = $('.sl-add-new');
    this.enableEditModeIcon = $('.sl-edit-mode');
    this.allItems = element.all(by.css('.sl-item-wrapper'));

    var utils = new Utils();

    this.initiateItemAdding = function() {
        utils.clickByElementWhenReady(this.addItemIcon);
    };

    this.enableEditMode = function() {
        utils.clickByElementWhenReady(this.enableEditModeIcon)
    };
};

module.exports = HomePage;