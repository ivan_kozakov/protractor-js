var Utils = require('../utils.js');

var WorkspaceSelectionPage = function() {

    var utils = new Utils();

    this.selectWorkspace = function(workspaceName) {
        utils.clickByLocatorWhenReady(by.linkText(workspaceName));
    }
};

module.exports = WorkspaceSelectionPage;